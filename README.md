# PagSeguro #

Classe PHP para trabalhar com o PagSeguro. Contém métodos para efetuar pagamento e checar notificações.

## Dependências ##
- Conta do PagSeguro ([pagseguro.com.br](http://pagseguro.com.br))
- Biblioteca cURL

## Exemplos ##
Exemplos de utilização da classe

### Efetuar Pagamento ###
É utilizado para registrar um pagamento.

```
#!php

<?php 
$pag = new PagSeguro('seu@email.com', 'TOKEN');
$pag->setReference('1234');
$pag->setSender('Nome Cliente', 'email@cliente.com', '63', '12345678');
$pag->setShippingAddress('Rua', '123', 'Complemento', 'Bairro', '77000000', 'Palmas', 'TO');
$pag->addItem(1, 'Tênis Fila', '159.00', 1);
$pag->setRedirectUrl('http://seusite.com/compra-realizada');
$pag->setNotificationUrl('http://seusite.com/alteracao-status');
$response = $pag->register();

if(!isset($response->error))
	echo $pag->getPaymentUrl($response->code);
else
	print_r($response);
```

## Alteração de Status ##
É utilizado para acompanhar a alteração de status do pagamento. Por exemplo, confirmação de pagamento ou cancelamento.

```
#!php
<?php
$code = (isset($_POST['notificationCode']) && trim($_POST['notificationCode']) !== ''  ? trim($_POST['notificationCode']) : null);
$type = (isset($_POST['notificationType']) && trim($_POST['notificationType']) !== ''  ? trim($_POST['notificationType']) : null);

$pag = new PagSeguro('seu@email.com', 'TOKEN');
$response = $pag->checkNotification($code, $type);
print_r($response);
```
