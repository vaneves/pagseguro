<?php
/**
 * Classe PHP para trabalhar com o PagSeguro. Contém métodos para efetuar pagamento e checar notificações.
 * 
 * @version		0.1
 * @author		Valdirene Neves <vaneves@vaneves.com>
 * @copyright	(c) 2013, Valdire Neves
 * @license		New BSD
 */
class PagSeguro
{
	const SHIPPING_PAC = 1;
	const SHIPPING_SEDEX = 2;
	const SHIPPING_UNKNOWN = 3;
	
	private $credentialEmail;
	private $credentialToken;
	private $url = 'https://ws.pagseguro.uol.com.br/';
	private $charset = 'UTF-8';
	private $currency = 'BRL';
	
	private $reference;
	
	private $senderName;
	private $senderEmail;
	private $senderAreaCode;
	private $senderPhone;
	
	private $items = array();
	
	private $shippingType;
	
	private $shippingAddressStreet;
	private $shippingAddressNumber;
	private $shippingAddressComplement;
	private $shippingAddressDistrict;
	private $shippingAddressPostalCode;
	private $shippingAddressCity;
	private $shippingAddressState;
	private $shippingAddressCountry;
	
	private $notificationUrl;
	private $redirectUrl;
	private $paymentUrl;
	
	private $connectionTimeout = 30;
	
	/**
	 * Construtor da classe.
	 * 
	 * @param	string	$email	E-mail da conta do PagSeguro.
	 * @param	string	$token	Token de segurança.
	 */
	public function __construct($email, $token)
	{
		$this->credentialEmail = $email;
		$this->credentialToken = $token;
		$this->shippingType = self::SHIPPING_UNKNOWN;
	}
	
	/**
	 * Define a codificação de caracteres usada nos parâmetros enviados.
	 * @param	string	$charset	Charset da codificação. Pode ser UTF-8 ou ISO-8859-1.
	 */
	public function setCharset($charset)
	{
		$this->charset = $charset;
	}
	
	/**
	 * Define a moeda na qual o pagamento será feito. No momento, a única opção disponível é BRL (Real).
	 * @param	string	$currency	Formato da moeda (ex. BRL).
	 */
	public function setCurrency($currency)
	{
		$this->currency = $currency;
	}
	
	/**
	 * Define um código para fazer referência ao pagamento.
	 * @param	string	$reference	Código de referência.
	 */
	public function setReference($reference)
	{
		$this->reference = $reference;
	}
	
	/**
	 * Define os dados do comprador.
	 * @param	string	$name		Nome do comprador.
	 * @param	string	$email		E-mail do comprador.
	 * @param	int		$areaCode	DDD do telefone do comprador (2 digitos).
	 * @param	int		$phone		Telefone do comprador (7 a 9 dígitos).
	 */
	public function setSender($name, $email, $areaCode = '', $phone = '')
	{
		$this->senderName = $name;
		$this->senderEmail = $email;
		$this->senderAreaCode = $areaCode;
		$this->senderPhone = $phone;
	}
	
	/**
	 * Adiciona um item na lista de pagamento.
	 * @param	strring	$id				Identificado do item.
	 * @param	string	$description	Descrição do item.
	 * @param	float	$amount			Preço unitário do item. Decimal, com duas casas decimais separadas por ponto (ex. 321.12).
	 * @param	int		$quantity		Quantidade de item.
	 * @param	int		$weight			Correspondem ao peso (em gramas) do item.
	 */
	public function addItem($id, $description, $amount, $quantity = 1, $weight = 0)
	{
		$this->items[] = array(
			'id' => $id,
			'description' => $description,
			'amount' => $amount,
			'quantity' => $quantity,
			'weight' => $weight,
		);
	}
	
	/**
	 * Define o tipo de frete a ser usado para o envio do produto.
	 * @param	int	$name	Tipo do frete. Utilize as constate da classe PagSeguro.
	 */
	public function setShippingType($type)
	{
		$this->shippingType = $type;
	}
	
	/**
	 * Dados do endereço de envio.
	 * @param	string	$street		Rua do endereço.
	 * @param	string	$number		Número (texto livre).
	 * @param	string	$complement	Complemento (bloco, apartamento, etc.).
	 * @param	string	$district	Bairro.
	 * @param	string	$postalCode	CEP (somente os números, 8 dígitos).
	 * @param	string	$city		Nome da cidade.
	 * @param	string	$state		Singla do estado (2 dígitos).
	 * @param	string	$country	Sigla do país (3 dígitos).
	 */
	public function setShippingAddress($street, $number, $complement, $district, $postalCode, $city, $state, $country = 'BRA')
	{
		$this->shippingAddressStreet = $street;
		$this->shippingAddressNumber = $number;
		$this->shippingAddressComplement = $complement;
		$this->shippingAddressDistrict = $district;
		$this->shippingAddressPostalCode = $postalCode;
		$this->shippingAddressCity = $city;
		$this->shippingAddressState = $state;
		$this->shippingAddressCountry = $country;
	}
	
	/**
	 * Define a URL para a qual o PagSeguro enviará os códigos de notificação relacionados ao pagamento.
	 * @param	string	$url	URL da página.
	 */
	public function setNotificationUrl($url)
	{
		$this->notificationUrl = $url;
	}
	
	/**
	 * Define a URL para a qual o comprador será redirecionado após finalizar o pagamento.
	 * @param	string	$url	URL da página.
	 */
	public function setRedirectUrl($url)
	{
		$this->redirectUrl = $url;
	}
	
	/**
	 * Define o tempo máximo da conexão.
	 * @param	int	$seconds	Tempo em segundos.
	 */
	public function setConnectionTimeout($seconds)
	{
		$this->connectionTimeout = $seconds;
	}
	
	/**
	 * Pega o endereço da página de pagamento para o usuário.
	 * @param	$code	Código da transação que é retornado pelo PagSeguro.
	 * @return	string	Retorna o endereço da página de pagamento.
	 */
	public function getPaymentUrl($code)
	{
		return $this->paymentUrl = 'https://pagseguro.uol.com.br/v2/checkout/payment.html?code=' . $code;
	}
	
	/**
	 * Registra um pagamento
	 * @return	object	Retorna um objeto do XML da resposta.
	 */
	public function register()
	{
		$params = array();
		
		$params['email'] = $this->credentialEmail;
		$params['token'] = $this->credentialToken;
		$params['currency'] = $this->currency;
		$params['reference'] = $this->reference;
		if($this->senderName)
			$params['senderName'] = $this->senderName;
		if($this->senderEmail)
			$params['senderEmail'] = $this->senderEmail;
		if($this->senderAreaCode && $this->senderPhone)
		{
			$params['senderAreaCode'] = $this->senderAreaCode;
			$params['senderPhone'] = $this->senderPhone;
		}
		$params['shippingType'] = $this->shippingType;
		if($this->shippingAddressStreet)
		{
			$params['shippingAddressStreet'] = $this->shippingAddressStreet;
			$params['shippingAddressNumber'] = $this->shippingAddressNumber;
			$params['shippingAddressComplement'] = $this->shippingAddressComplement;
			$params['shippingAddressDistrict'] = $this->shippingAddressDistrict;
			$params['shippingAddressPostalCode'] = $this->shippingAddressPostalCode;
			$params['shippingAddressCity'] = $this->shippingAddressCity;
			$params['shippingAddressState'] = $this->shippingAddressState;
			$params['shippingAddressCountry'] = $this->shippingAddressCountry;
		}
		
		foreach($this->items as $k => $v) 
		{
			$i = $k + 1;
			$params['itemId' . $i] = $v['id'];
			$params['itemDescription' . $i] = $v['description'];
			$params['itemAmount' . $i] = $v['amount'];
			$params['itemQuantity' . $i] = $v['quantity'];
			$params['itemWeight' . $i] = $v['weight'];
		}
		
		if($this->notificationUrl)
			$params['notificationURL'] = $this->notificationUrl;
		if($this->redirectUrl)
			$params['redirectURL'] = $this->redirectUrl;
		
		return $this->post($this->url . 'v2/checkout/', $params);
	}
	
	/**
	 * Faz a checagem de uma notificação no servidor do PagSeguro.
	 * @param	string	$code	Código da notificação.
	 * @param	int		$type	Tipo da notificação.
	 * @return	object	Retorna um objeto do XML da resposta.
	 */
	public function checkNotification($code, $type)
	{
		$params = array(
			'email' => $this->credentialEmail,
			'token' => $this->credentialToken,
		);
		return $this->get($this->url . 'v2/transactions/notifications/' . $code . '/', $params);
	}
	
	/**
	 * Faz uma requisição POST.
	 * @param	string	$url	URL da requisição.
	 * @param	array	$params	Parâmetros da requisição.
	 * @return	object	Retorna um objeto do XML da resposta.
	 */
	private function post($url, $params = array())
	{
		return $this->request($url, $params, 'POST');
	}
	
	/**
	 * Faz uma requisição GET.
	 * @param	string	$url	URL da requisição.
	 * @param	array	$params	Parâmetros da requisição.
	 * @return	object	Retorna um objeto do XML da resposta.
	 */
	private function get($url, $params = array())
	{
		$url .= '?' . http_build_query($params);
		return $this->request($url, array());
	}
	
	/**
	 * Faz uma requisição HTTP.
	 * @param	string	$url		URL da requisição.
	 * @param	array	$params		Parâmetros da requisição.
	 * @param	string	$method		Tipo (GET ou POST).
	 * @return	object	Retorna um objeto do XML da resposta.
	 * @throws	Exception	Disparada caso ocorra algum erro.
	 */
	private function request($url, $params = array(), $method = 'GET')
	{
		if(!function_exists('curl_init'))
			throw new Exception('É necessário habilitar a biblioteca "cURL"');
		
		$headers = array(
			'Content-Type: application/x-www-form-urlencoded; charset=' . $this->charset,
			'Content-length: '. strlen(http_build_query($params))
		);
		
		$curl_options = array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => $method,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_POST => $method == 'POST',
			CURLOPT_POSTFIELDS => http_build_query($params),
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_CONNECTTIMEOUT => $this->connectionTimeout,
			CURLOPT_SSL_VERIFYPEER => false
		);
		
		$ch = curl_init();
		curl_setopt_array($ch, $curl_options);

		$response = curl_exec($ch);
		$error_code = curl_errno($ch);
		$error_message = curl_error($ch);
		curl_close($ch);
		
		if($error_code)
			throw new Exception('cURL error: ['. $error_code .']' . $error_message);
		
		if(!$response)
			throw new Exception('A conexão retornou um resultado vazio');
		
		$xml = simplexml_load_string($response);
		if(!is_object($xml))
			throw new Exception('A conexão retornou um resultado inválido');
			
		return $xml;
	}
}
